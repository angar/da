/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: usuario
 *
 * Created on 2 de junio de 2019, 18:37
 */

#include <stdio.h>
#include <stdlib.h>
#include "ivector.h"

#define TAM_V_MONEDAS 4
#define TAM_V_NUM_MONEDAS 4
#define CANTIDAD_TOTAL 138

int monedas[TAM_V_MONEDAS]= {25,10,5,1};


int greedy_Monedas(ivector v, int cantidad);
int solucion (ivector v, int cantidad);
int factible (ivector v, int cantidad);

void mostrarVector(ivector v, int tam);
/*
 * 
 */
int main(int argc, char** argv) {
    
    //Creamos un vector para saber la cantidad de cada moneda,
    //Lo hacemos dinamico porque asi podemos hacerlos para cualesquiera monedas
    
    ivector v;
    v = icreavector(TAM_V_NUM_MONEDAS);
    for (int i=0; i< TAM_V_NUM_MONEDAS; i++)
        v[i]=0;
    
    int n_mo= greedy_Monedas(v, CANTIDAD_TOTAL);
    printf("Cantidad: %d -> numero monedas: %d\n", CANTIDAD_TOTAL, n_mo);
    
    printf("Vector monedas         -> ");
    mostrarVector(monedas, TAM_V_MONEDAS);
    printf("Vector cantidad monedas-> ");
    mostrarVector(v, TAM_V_NUM_MONEDAS);    
    
    
    
    ifreevector(&v);
    
    return (EXIT_SUCCESS);
}

int greedy_Monedas(ivector v, int cantidad){
    int suma = 0;
    int i=0;
    int cantidadMo=0;
    
    while(!solucion (v, cantidad) ){//while(solucion (v, cantidad) != 1 ){
        v[i] += 1;//Seleccionamos una moneda mas
        ++cantidadMo;
        //Si no es factible saco la moneda del vector de cantidades
        if (!factible(v,cantidad) ){
            v[i++] -= 1;//Primero resta 1 moneda porque no nos vale y luego queda en la siguiente moneda
            //++i;
            --cantidadMo;
        }
    }
    return cantidadMo;
    
}

/**
 * @brief Funcion para comprobar si la suma que llevamos es solucion
 * @param v
 * @param cantidad
 * @return 
 */
int solucion (ivector v, int cantidad){
    int suma = 0;
    for (int i=0; i< TAM_V_NUM_MONEDAS; i++)
        suma+= (v[i]* monedas[i]);
    return (suma == cantidad) ? 1 : 0;//return (suma == cantidad) ? 1 : -1;
        //Nota, en C no hay true o false, en C 0->false y cualquier numero distinto 
        //de 0 es true
        //Cero es falso y cualquier valor distinto de cero es verdadero
}

/**
 * @brief Funcion para comprobar si es factible utilizar la moneda 
 *        selecionada
 * @param v vector de cantidades de monedas
 * @param cantidad cantidad total a devolver
 * @return 
 */
int factible (ivector v, int cantidad){
    int suma = 0;
    for (int i=0; i< TAM_V_NUM_MONEDAS; i++)
        suma+= (v[i]*monedas[i]);
    return (suma <= cantidad) ? 1 : 0;//return (suma <= cantidad) ? 1 : -1;
}



/**
 * @brief Funcion para mostrar el vector
 * @param v vector de enteros
 * @param tam tamaño del vector
 */
void mostrarVector(ivector v, int tam){
    
    for (int i = 0; i< tam; i++)
        printf ("%d\t", v[i]);
    printf ("\n");
}
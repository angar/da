/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: usuario
 *
 * Created on 3 de junio de 2019, 15:38
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "ivector.h"

#define IESIMO 40
long int fibRecc(int iesimo);
long int fib(int iesimo);
long int fib2(int iesimo);
long int fib3(int iesimo);
/*
 * 
 */
int main(int argc, char** argv) {
    
    long int valor;
    clock_t start, end;
    double cpu_time_user;
    
    start = clock();
    valor = fibRecc(IESIMO);
    end = clock();
    cpu_time_user = ((double) end - start) / CLOCKS_PER_SEC;
    printf("Fibo recurs  : Iesimo: %ld, valor: %d---Tiempo: %f\n", valor, IESIMO, cpu_time_user);
    
    start = clock();
    valor = fib(IESIMO);
    end = clock();
    cpu_time_user = ((double) end - start) / CLOCKS_PER_SEC;
    printf("Fibo proDin1 : Iesimo: %ld, valor: %d---Tiempo: %f\n", valor, IESIMO, cpu_time_user);
    
    start = clock();
    valor = fib2(IESIMO);
    end = clock();
    cpu_time_user = ((double) end - start) / CLOCKS_PER_SEC;
    printf("Fibo proDin2 : Iesimo: %ld, valor: %d---Tiempo: %f\n", valor, IESIMO, cpu_time_user);    
    
    start = clock();
    valor = fib3(IESIMO);
    end = clock();
    cpu_time_user = ((double) end - start) / CLOCKS_PER_SEC;
    printf("Fibo proDin3 : Iesimo: %ld, valor: %d---Tiempo: %f\n", valor, IESIMO, cpu_time_user);      
    
    
    
    
    /*
     * Vemos lo ineficiente que es el recursivo, habria que utilizar un vector
     * de long int para hacer el calculo porque aunque devolvamos un long int
     * la posicion del vector es un int entonces desbordara.
    Fibo recurs  : Iesimo: 102334155, valor: 40---Tiempo: 3.020537
    Fibo proDin1 : Iesimo: 102334155, valor: 40---Tiempo: 0.000004
    Fibo proDin2 : Iesimo: 102334155, valor: 40---Tiempo: 0.000002
    Fibo proDin3 : Iesimo: 102334155, valor: 40---Tiempo: 0.000002
     * 
     */
    return (EXIT_SUCCESS);
}



/**
 * @brief MEtodo para calculo sucession Fibonacci recursivo
 * @param iesimo
 * @return 
 */
long int fibRecc(int iesimo){
    if (iesimo<=1)
        return iesimo;
    else
        return fibRecc(iesimo-1) + fibRecc(iesimo-2);
}


/**
 * @brief MEtodo para calculo sucession Fibonacci con con programacion dinamica
 *        resolvemos 1 posiciones en cada iteracion
 * @param iesimo
 * @return 
 */
long int fib(int iesimo){
    ivector v;
    
    v = icreavector(iesimo+1);
    
    v[0] = 0;
    v[1] = 1;
    
    for (int i= 2; i<=iesimo; i++)
        v[i]  = v[i-1]+v[i-2];
    
    int result= v[iesimo];
    ifreevector(&v);
    return result;     
}



/**
 * @brief MEtodo para calculo sucession Fibonacci con con programacion dinamica
 *        resolvemos 2 posiciones en cada iteracion
 * @param iesimo
 * @return 
 */
long int fib2(int iesimo){
    ivector v;
    
    v = icreavector(iesimo+1);
    
    v[0] = 0;
    v[1] = 1;
    
    for (int i= 2; i<=iesimo; i+=2){
        v[i]  = v[i-1]+v[i-2];
        v[i+1]= v[i]+v[i-1];
        
    }
    
    int result= v[iesimo];
    ifreevector(&v);
    return result;      
}



/**
 * @brief MEtodo para calculo sucession Fibonacci con con programacion dinamica
 *        resolvemos 3 posiciones en cada iteracion
 * @param iesimo
 * @return 
 */
long int fib3(int iesimo){
    ivector v;
    
    v = icreavector(iesimo+1);
    
    v[0] = 0;
    v[1] = 1;
    
    for (int i= 2; i<=iesimo; i+=3){
        v[i]  = v[i-1] +  v[i-2];
        v[i+1]= v[i]   +  v[i-1];
        v[i+2]= v[i+1] +  v[i];
        
    }
    
    int result= v[iesimo];
    ifreevector(&v);
    return result;      
}
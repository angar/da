/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: usuario
 *
 * Created on 3 de junio de 2019, 16:48
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "imatriz2d.h"

#define N 50
#define M 3

long int binomialRecursivo (int n, int m);
long int binomialPD (int n, int m);
void mostrarMatriz (imatriz2d m, int f, int c);
/*
 * 
 */
int main(int argc, char** argv) {
    
    long int valor;
    clock_t start, end;
    double cpu_time_user;
    
    start = clock();
    valor = binomialRecursivo(N,M);
    end = clock();
    cpu_time_user = ((double) end - start) / CLOCKS_PER_SEC;
    printf("Binomial recurs  : N: %d, M: %d, Resultado: %ld ---Tiempo: %f\n", N, M, valor, cpu_time_user);
    

    start = clock();
    valor = binomialPD(N,M);
    end = clock();
    cpu_time_user = ((double) end - start) / CLOCKS_PER_SEC;
    printf("Binomial ProgDi  : N: %d, M: %d, Resultado: %ld ---Tiempo: %f\n", N, M, valor, cpu_time_user);

    
    
    /*
     * Vemos lo ineficiente que es el recursivo, habria que utilizar un vector
     * de long int para hacer el calculo porque aunque devolvamos un long int
     * la posicion del vector es un int entonces desbordara.
    Fibo recurs  : Iesimo: 102334155, valor: 40---Tiempo: 3.020537
    Fibo proDin1 : Iesimo: 102334155, valor: 40---Tiempo: 0.000004
    Fibo proDin2 : Iesimo: 102334155, valor: 40---Tiempo: 0.000002
    Fibo proDin3 : Iesimo: 102334155, valor: 40---Tiempo: 0.000002
     * 
     */
    return (EXIT_SUCCESS);
}



/**
 * @brief MEtodo para calculo binomial
 * @param n
 * @param m
 * @return 
 */
long int binomialRecursivo (int n, int m){
    
    if( m == 0 || n ==m)
        return 1;
    else
        return binomialRecursivo(n-1,m) + binomialRecursivo(n-1,m-1);
/*

   //Otra forma 
   //https://es.wikipedia.org/wiki/Coeficiente_binomial
    if (m == 0)
        return 1;
    else if(n == 0)
        return 0;
    else
        return binomialRecursivo(n-1,m) + binomialRecursivo(n-1,m-1);
    
*/
/*
    //Otra forma buena
    if (m== 0 )
        return 1;
    else if(n == m)
        return 1;
    else
        return binomialRecursivo(n-1,m) + binomialRecursivo(n-1,m-1);
        
*/



}

/**
 * @brief MEtodo para calculo binomial
 * @param n
 * @param m
 * @return 
 */
long int binomialPD (int n, int m){
    
    //Consideramos la n como filas y la m como columnas
    imatriz2d mat;
    mat = icreamatriz2d(n+1,m+1);
    



    for (int i = 0; i <=n; i++) 
        for (int j = 0; j <=m; j++)
            mat[i][j] = 0;
        
    for (int i=0; i<=n;i++)
        mat[i][0]=1;
    
    //Aqui cuidado la matriz no es cuadrada , solo hasta m porque
    //si ponemos n como n siempre es mayor o igual que m se sale de la matriz
    for (int i=1; i<=m;i++)
        mat[i][i]=1;
        
    //mostrarMatriz(mat, n+1,m+1);
    
    for (int j = 1; j <=m; j++) 
        for (int i = j+1; i <=n; i++)
            mat[i][j]= mat[i-1][j]+ mat[i-1][j-1];
    
    //mostrarMatriz(mat, n+1,m+1);
    int result = mat[n][m];

    ifreematriz2d(&mat);
    
    return result;
}


void mostrarMatriz (imatriz2d m, int f, int c){
    for (int i=0; i<f;i++){
        for (int j=0; j<c;j++)
            printf("%d\t", m[i][j]);
        printf("\n");
    }
    printf("\n");
    
}


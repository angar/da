/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: usuario
 *
 * Created on 15 de mayo de 2019, 11:55
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>//INT_MAX

#include "imatriz2d.h"


int monedas (int , int );
void mostrarMatriz(imatriz2d m, int f, int c);
/*
 * 
 */
int main(int argc, char** argv) {
    
    printf ("Monedas totales: %d", monedas (4, 27));
    return (EXIT_SUCCESS);
}

int monedas (int n , int c ){
    imatriz2d m;
    m = icreamatriz2d(n, c);
    
    
    int monedas[] = {0,1,4,6};
    
    for (int j = 1; j < c; j++)
        m[0][j] = INT_MAX;
    for (int i= 0; i < n; i++)
        m[i][0] = 0;
    
    for (int i = 1; i< n; i++ )
        for (int j = 1; j < c; j++)
            if (j < monedas[i])//if (j < i)
                m[i][j]=m[i-1][j];
            else{//if j>= monedas[i]
                
                m[i][j] = (m[i-1][j] < 1+ m[i][j-monedas[i]]) ? m[i-1][j] : 1+ m[i][j-monedas[i]];
                
            }
    
    mostrarMatriz(m,n, c);
    
    int cantM []={0,0,0,0};
    
    int i = n-1;
    int j = c-1;
    
    while (j> 0){
        if (m[i][j] != m[i-1][j]){
            cantM[i] = cantM[i]+1;
        j = j - monedas[i];
        }else    
            i--;///Se sube arriba    
    }
    for (int i = 0; i< 4; i++)
        printf("%d\t",cantM[i] );
    
    
    
    int cantidad= m[n-1][c-1];
    ifreematriz2d(&m);
    return cantidad;
}
void mostrarMatriz(imatriz2d m, int f, int c) {
    for (int i = 0; i < f; i++) {
        for (int j = 0; j < c; j++) {
            printf("%d\t", m[i][j]);
        }
        printf("\n");
    }
}
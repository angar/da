/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: admin
 *
 * Created on 14 de marzo de 2019, 9:23
 * 
 * https://www.geeksforgeeks.org/merge-sort/
 * https://www.sanfoundry.com/cpp-program-implement-merge-sort/
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TAM 101
#define UMBRAL 25
typedef int vector[TAM];
void mergesort (vector  a, int l, int r);
void merge(vector a, int low, int high, int mid);
/*
 * 
 */
int main(int argc, char** argv) {
    srand(time(NULL));

    vector v;
    for (int i = 0; i < TAM; i++) {
        v[i] = rand() % 100;
    }

    for (int i = 0; i < TAM; i++) {
        printf("%d \t", v[i]);
    }

    mergesort(v, 0, TAM -1);

    printf("\n \n");
    for (int i = 0; i < TAM; i++) {
        printf("%d \t", v[i]);
    }
    return (EXIT_SUCCESS);
}



void mergesort (vector a, int low, int high){
    //if (tam < UMBRAL)
        //Algoritmo clasico
    
    int mid;
    
    if (low < high){
        mid = (low+high)/2;
        
        mergesort(a, low, mid);
        mergesort(a, mid+1, high);
        
        merge(a, low, high, mid);
    }
    
}

void merge(vector a, int low, int high, int mid){
    int i,j,k, temp[high-low+1];
    i= low;
    k = 0;
    j = mid + 1;
    
    while (i <= mid && j <= high){
        if (a[i] < a[j]){
            temp[k] = a[i];
            k++;
            i++;
        }else{
            temp[k]= a[j];
            k++;
            j++;
        }
    }
    
    while (i <= mid){
        temp[k] = a[i];
        k++;
        i++;
    }
    
    while (j <= high){
        temp[k] = a[j];
        k++;
        j++;
    }
    
    for (i=low; i<=high; i++)
        a[i] = temp[i-low];
    
    
}
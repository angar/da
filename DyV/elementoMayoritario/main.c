/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: administrador
 *
 * Created on 21 de marzo de 2019, 9:29
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "ivector.h"

#define Val_Max_Num_Aleatorio 100
#define Tam_ivector 20
#define N_0 5//DEFINIMOS EL UMBRAL EN UN TAMAÑO DE 50

void cargarVector(ivector, int);
void mostrarVector(ivector, int);
int mayoritario(ivector v, int i, int j);


/*
 * 
 */
int main(int argc, char** argv) {
    
    ivector v;
    
    v = icreavector( Tam_ivector);
    
    cargarVector(v,Tam_ivector);
    
    mostrarVector(v,Tam_ivector);
    
    
    
    return (EXIT_SUCCESS);
}

int mayoritario(ivector v, int i, int j){
    //(j-i)+1) esto nos dice el tamaño de esa parte
    if ((j-i)+1 > N_0)
        //Resolver
            return 0;
    else{
    int mitad = (j + i) / 2;
    mayoritario(v, i, mitad);
    mayoritario(v, mitad + 1, j);
    
    }
}


/**
 * @brief Funcion para cargar el vector con numero aleatorios
 * @param v
 * @param tam
 */
void cargarVector(ivector v, int tam){
    srand(time(NULL));
    for (int i = 0; i < tam; i++){
        v[i] = (rand() % Val_Max_Num_Aleatorio) + 1;// Seran numero comprendidos entre 1 y Val_Max
    }
}


void mostrarVector(ivector v, int tam){
    
    printf("Vector: \n");
    for (int i = 0; i < tam; i++){
        printf("%d\t", v[i]);
    }
    printf("\n");
}

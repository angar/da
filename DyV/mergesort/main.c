/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: usuario
 *
 * Created on 20 de marzo de 2019, 15:35
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h> //Para inicializar la semilla se srand
#include "ivector.h"
#define N_0 5//DEFINIMOS EL UMBRAL EN UN TAMAÑO DE 50

#define Val_Max_Num_Aleatorio 100
#define Tam_ivector 20

/*
 * 
 */

void mergesort(ivector T, ivector W, int i, int j);
void merge (ivector t, ivector w, int lim_i, int mitad, int lim_j);
//void insercionDirecta (ivector , int );
void insercionDirecta (ivector vect, int lim_i, int lim_s);
void cargarVector(ivector, int);
void mostrarVector(ivector, int);

int main(int argc, char** argv) {

/*
    ivector v;
    v = icreavector(Tam_ivector);
    cargarVector(v, Tam_ivector);
    mostrarVector(v, Tam_ivector);
    printf("\n\n");
    insercionDirecta(v, 0, Tam_ivector-1);
    mostrarVector(v, Tam_ivector);
    ifreevector (&v);
*/
    clock_t start, end;
    double cpu_time_user;
    
    ivector v,w;
    v = icreavector(Tam_ivector);
    w = icreavector(Tam_ivector);
    cargarVector(v, Tam_ivector);
    mostrarVector(v, Tam_ivector);
    printf("\n\n");
    start = clock();
    mergesort(v, w, 0, Tam_ivector-1);
    end = clock();
    
    cpu_time_user = ((double) end - start) / CLOCKS_PER_SEC;
    
    printf("Tiempo: %f", cpu_time_user);
    
    /*
     Tiempo: 0.014572    N_0 5   Tam_ivector 50000
     * 
     * Tiempo: 0.013634  N_0 10  Tam_ivector 50000
     * 
     * Tiempo: 0.012953  N_0 20  Tam_ivector 50000
     * 
     * Tiempo: 0.012469  N_0 30  Tam_ivector 50000
     * 
     * Tiempo: 0.012401  N_0 40  Tam_ivector 50000
     * 
     * Tiempo: 0.013026  N_0 50  Tam_ivector 50000
     * 
     * Tiempo: 0.013035  N_0 60  Tam_ivector 50000
     * 
     * Tiempo:  0.015605 N_0 100  Tam_ivector 50000

     */
    mostrarVector(v, Tam_ivector);
    //mostrarVector(w, Tam_ivector);
    ifreevector (&v);    
    ifreevector (&w); 
    return (EXIT_SUCCESS);
}


/**
 * 
 * @param T vector de enteros
 * @param W vector de trabajo
 * @param i indice inferior del vector
 * @param j indice superior del vector
 */
void mergesort(ivector t, ivector w, int i, int j){
    
    if ((j-i)+1 < N_0){
        //Algoritmo clasico ***INSERCION DIRECTA***
        //insercionDirecta(w, (j-i));
        insercionDirecta(t, i, j); ///EL ALGORITMO CLASICO HAY QUE HACERLO SOBRE EL VECTOR DE TRABAJO Y LUEGO COPIARLO AL VECTOR INICIAL T???
        //////mostrarVector(t, Tam_ivector);//(j-i)+1
    }else{
        int mitad = (j + i) / 2;//printf("mitad %d", mitad);
        mergesort(t, w, i, mitad);
        mergesort(t, w, mitad + 1, j);
        merge (t,w,i, mitad, j);
            
    }
            
}

void merge (ivector t, ivector w, int lim_i, int mitad, int lim_j){
    int i = lim_i;
    int j = mitad+1;
    int k = lim_i;
    
    //printf("holasadsasad\n");
    ////mostrarVector(t, Tam_ivector);//(j-i)+1
    ////mostrarVector(w, Tam_ivector);//(j-i)+1
    while (i <= mitad && j <= lim_j){
        if (t[i] <= t[j]){
            w[k] = t[i];
            i++;
        }else{
            w[k] = t[j];
            j++;
        }
        k = k+1; 
    }
    
    if (i > mitad) {
        for (; j <= lim_j; j++) {
            w[k] = t[j];
            k++;
        }
    } else {
        for (; i <= mitad; i++) {
            w[k] = t[i];
            k++;
        }
    }
        //printf("hola\n");
    ////mostrarVector(t, Tam_ivector);//(j-i)+1
    ////mostrarVector(w, Tam_ivector);//(j-i)+1
    //Copiamos el vector w en t desde i hasta j
    for (i = lim_i; i <= lim_j; i++){
        t[i] = w[i];
    }
        
}

/**
void Insercion(int numbers[], int array_size) {
  int i, a, index;
  for (i=1; i < array_size; i++) {
    index = numbers[i];
    a = i-1;
    while (a >= 0 && numbers[a] > index) {
      numbers[a + 1] = numbers[a];
      a--;
    }
    numbers[a+1] = index;
  }
}
 */

/*
/**
 * @brief Algoritmo de ordenador de insercion Directa
 * @param vect vector de tipo ivector
 * @param tam tamaño del vector
void insercionDirecta (ivector vect, int tam){
    int i,j,v;
    
    for (i =1; i < tam; i++){
        v = vect[i];
        j = i -1;
        while (j >= 0 && vect[j] > v){
            vect[j+1] = vect[j];
            j--;
        }
        vect[j+1]=v;
    }
    
}
*/

/**
 * @brief Algoritmo de ordenador de insercion Directa
 * @param vect vector de tipo ivector
 * @param tam tamaño del vector
 */
void insercionDirecta (ivector vect, int lim_i, int lim_s){
    
    //int tam = (lim_s - lim_i)+1; 
    int i,j,v;
    
    for (i = lim_i + 1; i <= lim_s; i++){
        v = vect[i];
        j = i -1;
        while (j >= lim_i && vect[j] > v){
            vect[j+1] = vect[j];
            j--;
        }
        vect[j+1]=v;
    }
    
}




/**
 * @brief Funcion para cargar el vector con numero aleatorios
 * @param v
 * @param tam
 */
void cargarVector(ivector v, int tam){
    srand(time(NULL));
    for (int i = 0; i < tam; i++){
        v[i] = (rand() % Val_Max_Num_Aleatorio) + 1;// Seran numero comprendidos entre 1 y Val_Max
    }
}


void mostrarVector(ivector v, int tam){
    
    printf("Vector: \n");
    for (int i = 0; i < tam; i++){
        printf("%d\t", v[i]);
    }
    printf("\n");
}

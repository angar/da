/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: usuario
 *
 * Created on 2 de junio de 2019, 17:37
 */

#include <stdio.h>
#include <stdlib.h>
#include "ivector.h"

#define TAMVECTOR 10//pARA EL DE CLASE TAMAÑO 11
#define POS_K 7


int k_esimo (ivector v, int i, int j, int k);
int encuentraPivote (ivector v, int i, int j);
int reordena (ivector v, int i, int d, int pivote);

//Prototipos funciones auxiliares
void mostrarVector (ivector v, int tam);
void cargarVectorValAleatorios(ivector v, int tam);


//Prototipo funcion
 void ordenacionInsercion (ivector v, int i, int j);

/*
 * 
 */
int main(int argc, char** argv) {
    
    
    
    ivector v;
    v = icreavector(TAMVECTOR);
    cargarVectorValAleatorios(v, TAMVECTOR);
    printf("VECTOR *ANTES* DE BUSCAR K_ESIMO\n");
    mostrarVector(v, TAMVECTOR);
    int valor= k_esimo(v,0, TAMVECTOR-1, POS_K-1);
    
    
    printf("\nK: %d-> valor: %d\n", POS_K, valor);
    printf("VECTOR *DESPUES* DE BUSCAR K_ESIMO\n");
    mostrarVector(v, TAMVECTOR);
    
    
     ivector v1;
    v1 = icreavector(TAMVECTOR);
    cargarVectorValAleatorios(v1, TAMVECTOR);
    printf("VECTOR *ORDENADO* POR INSERCION, para ver la posicion estando ordenado\n");
    
    ordenacionInsercion (v1,0, TAMVECTOR-1);
     mostrarVector(v1, TAMVECTOR);
     
    ifreevector(&v);
    ifreevector(&v1);
    return (EXIT_SUCCESS);
}



int k_esimo (ivector v, int i, int j, int k){
    int pivote;
    
    if (i==j) 
        return v[i];
    else{
        pivote = encuentraPivote(v, i,j);
        if (pivote == -1) 
            return v[i];
        pivote = reordena(v, i,j, v[pivote]);
        if (k< pivote) 
            return k_esimo(v, i, pivote-1, k);
        else
            return k_esimo(v,pivote,j,k);
    }
}




/**
 * @brief Empezando por la izquierda, devuelve la posicion 
 *        del mayor de los dos primeros elementos que sean distintos
 * @param v Vector de enteros
 * @param i 
 * @param j
 * @return Devuelve la poscion del pivote
 */
int encuentraPivote (ivector v, int i, int j){
    int k;
    
    for (k=i+1; k<=j;k++){
        if (v[k]> v[i])
            return k;
        else if (v[k]<v[i]) 
            return i;
    }
    return -1;
        
}

/**
 * @brief Deja los elementos menores que el pivote a la izquierda
 *        y los mayores o iguales a la derecha. Devuelve la posicion
 *        de corte
 * @param v 
 * @param i
 * @param d
 * @param pivote Valor del pivote
 * @return Devuelve la posicion de corte
 */
int reordena (ivector v, int i, int d, int pivote){
    
    do{
        int temp;
        temp = v[i];
        v[i] = v[d];
        v[d] = temp;

/*
#ifdef PRINTF 
    mostrarVector(v,TAMVECTOR);
#endif
*/
        while (v[i] < pivote) i++;
        while (v[d] >= pivote) d--;
    }while (i <= d);

/*
#ifdef PRINTF 
    mostrarVector(v,TAMVECTOR);
#endif   
*/


    return i;
}



/**
 * @brief Funcion para mostrar el vector
 * @param v vector de enteros
 * @param tam tamaño del vector
 */
void mostrarVector(ivector v, int tam){
    
    for (int i = 0; i< tam; i++)
        printf ("%d\t", v[i]);
    printf ("\n");
}




/**
 * @brief Funcion para cargar un vector de entero de cualquier tamaño la semilla
 *        es 23 
 * @param v
 * @param tam
 */
void cargarVectorValAleatorios(ivector v, int tam){
    srand(23);//srand(time(NULL));
    for (int i= 0; i< tam; i++)
        v[i] = rand()%1000;
}



/**
 * @brief Algoritmo clasico de ordenacion por insercion
 *        Utilizado para ordenadar dado un umbral
 * @param v
 * @param i
 * @param j
 */
void ordenacionInsercion (ivector v, int i, int j){
    
    int p, k;
    int tmp;
    
    for (p = i; p <= j; p++){
        tmp = v[p];
        k = p-1; 
        while ((k>=0) && (tmp < v[k])){
            v[k+1] = v[k];
            k--;
        }
        v[k+1] = tmp;
    }
    
    
    
}
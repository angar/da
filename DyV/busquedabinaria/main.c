/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: usuario
 *http://www.lcc.uma.es/~av/Libro/CAP3.pdf
 * https://ocw.ehu.eus/pluginfile.php/9409/mod_resource/content/1/02_Divide_y_Venceras/02_Divide_y_Venceras_v2.pdf
 * https://es.khanacademy.org/computing/computer-science/algorithms/merge-sort/a/divide-and-conquer-algorithms
 * Created on 20 de marzo de 2019, 20:07
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h> 
#include "ivector.h"


#define Tam_ivector 30
#define Val_Max_Num_Aleatorio 100


void insercionDirecta (ivector vect, int tam);
void cargarVector(ivector, int);
void mostrarVector(ivector, int);
int busquedaBinariaDyV (ivector t, int inf, int sup, int dato);
int busquedaBinaria(ivector t,int tam, int dato);

/*
 * 
 */
int main(int argc, char** argv) {

    ivector v;
    v = icreavector(Tam_ivector);
    cargarVector(v, Tam_ivector);
    mostrarVector(v, Tam_ivector);
    insercionDirecta (v, Tam_ivector);
    mostrarVector(v, Tam_ivector);
    
    int pos = busquedaBinariaDyV(v, 0, Tam_ivector-1, 20);
    printf("POs: %d\n", pos);
    
    
    return (EXIT_SUCCESS);
}


int busquedaBinariaDyV(ivector t, int inf, int sup, int dato){
    //int inf = 0;
    //int sup = tam -1;
    int centro;

    if (inf > sup)
        return -1;
    else{
    centro = (inf + sup) / 2;
    
    if (t[centro] == dato)
        return centro;
    else if ( dato > t[centro])
        busquedaBinariaDyV(t, centro + 1, sup, dato);
    else
        busquedaBinariaDyV(t, inf, centro-1, dato);//inf = curIn + 1;//Hay que poner -1 que no lo ponia
    }
    
}

int busquedaBinaria(ivector t, int tam, int dato){
    int inf = 0;
    int sup = tam -1;
    int curIn;
    
    while(inf <= sup){
        curIn = (inf + sup)/  2;
        if (t[curIn] == dato)
            return curIn;
        else if (t[curIn] < dato)
            inf = curIn +1;
        else
            sup= curIn -1;
    }
    return -1;
}

/**
 * @brief Algoritmo de ordenador de insercion Directa
 * @param vect vector de tipo ivector
 * @param tam tamaño del vector
 */
void insercionDirecta (ivector vect, int tam){
    int i,j,v;
    
    for (i =1; i < tam; i++){
        v = vect[i];
        j = i -1;
        while (j >= 0 && vect[j] > v){
            vect[j+1] = vect[j];
            j--;
        }
        vect[j+1]=v;
    }
    
}

void cargarVector(ivector v, int tam){
    srand(time(NULL));
    for (int i = 0; i < tam; i++){
        v[i] = (rand() % Val_Max_Num_Aleatorio) + 1;// Seran numero comprendidos entre 1 y Val_Max
    }
}


void mostrarVector(ivector v, int tam){
    
    printf("Vector: \n");
    for (int i = 0; i < tam; i++){
        printf("%d\t", v[i]);
    }
    printf("\n");
}

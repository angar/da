
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: usuario
 *
 * Created on 1 de junio de 2019, 17:24
 */


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>//
#include "ivector.h"


#define TAMVECTOR 10//pARA EL DE CLASE TAMAÑO 11
#define UMBRAL 15


void minMax (ivector v, int inf, int sup, int *min, int *max);
void minMax2(ivector v, int inf, int sup, int *min, int *max);
//Prototipos funciones auxiliares
void mostrarVector (ivector v, int tam);
void cargarVectorValAleatorios(ivector v, int tam);


/*
 * 
 */
int main(int argc, char** argv) {
    
    
    ivector v;
    int min= INT_MAX;
    int max= INT_MIN;
    v = icreavector(TAMVECTOR);
    cargarVectorValAleatorios(v, TAMVECTOR);
    mostrarVector(v, TAMVECTOR);
    minMax2 (v, 0, TAMVECTOR-1, &min, &max);
    
    printf("Mayor: %d, Menor: %d", max,min);
    //mostrarVector(v, TAMVECTOR);

    
    ifreevector(&v);
    return (EXIT_SUCCESS);
}

/**
 * @brief Funcion para buscar el maximo y el minimo de un vector
 *        no tiene porque estar ordenado.
 *        el umbral seria 1 es decir, va dividiendo el subpartes el vector
 *        hasta que la particion es 1, 
 * @param v
 * @param inf
 * @param sup
 * @param min
 * @param max
 */
void minMax(ivector v, int inf, int sup, int *min, int *max) {
    int medio, izqmin/*= INT_MAX*/, izqmax/*=INT_MIN*/;

    if (inf == sup) {
        *min = v[inf];
        *max = v[inf];
    } else {
        medio = (inf + sup) / 2;
        minMax(v, inf, medio, &izqmin, &izqmax);
        minMax(v, medio + 1, sup, min, max);
        if (izqmin<*min)
            *min = izqmin;
        if (izqmax>*max)
            *max = izqmax;
    }



}



/**
 * @brief Funcion para buscar el maximo y el minimo de un vector
 *        no tiene porque estar ordenado.
 *        el umbral seria 2 es decir, va dividiendo el subpartes el vector
 *        hasta que la particion es 2 
 * @param v
 * @param inf
 * @param sup
 * @param min
 * @param max
 */
void minMax2(ivector v, int inf, int sup, int *min, int *max) {
    int medio, izqmin/*= INT_MAX*/, izqmax/*=INT_MIN*/;

    if (inf == sup) {
        *min = v[inf];
        *max = v[inf];
    }else if(inf+1==sup){
        if (v[inf]<v[sup]){
            *min=v[inf];
            *max=v[sup];
        }else{
            *min=v[sup];
            *max=v[inf];
        }
    } else {
        medio = (inf + sup) / 2;
        minMax(v, inf, medio, &izqmin, &izqmax);
        minMax(v, medio + 1, sup, min, max);
        if (izqmin<*min)
            *min = izqmin;
        if (izqmax>*max)
            *max = izqmax;
    }



}





/**
 * @brief Funcion para mostrar el vector
 * @param v vector de enteros
 * @param tam tamaño del vector
 */
void mostrarVector(ivector v, int tam){
    
    for (int i = 0; i< tam; i++)
        printf ("%d\t", v[i]);
    printf ("\n");
}


/**
 * @brief Funcion para cargar un vector de entero de cualquier tamaño la semilla
 *        es 23 
 * @param v
 * @param tam
 */
void cargarVectorValAleatorios(ivector v, int tam){
    srand(50);//srand(time(NULL));
    for (int i= 0; i< tam; i++)
        v[i] = rand()%20;
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: usuario
 *
 * Created on 31 de mayo de 2019, 13:56
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h> //time, funcion para random

#include "ivector.h"

#define TAMVECTOR 5000//pARA EL DE CLASE TAMAÑO 11
#define UMBRAL 15

//#define PRINTF

//#define QUICKSORTUMBRAL
#define QUICKSORTSINUMBRAL
//#define ORDENACIONINSERCION

//Prototipos funciones del quicsort
void quicksort (ivector v, int i, int j);
void quicksortSinUmbral (ivector v, int i, int j);
int encuentraPivote (ivector v, int i, int j);
int reordena (ivector v, int i, int d, int pivote);

//Prototipos funciones auxiliares
void mostrarVector (ivector v, int tam);

//Prototipo funcion comparacion para qsort funcion de la biblioteca
int compare(const void *_a, const void *_b);

//Prototipo funcion
 void ordenacionInsercion (ivector v, int i, int j);
void cargarVectorValAleatorios(ivector v, int tam);
/*
 * 
 */
int main(int argc, char** argv) {
    
    clock_t start, end;
    double cpu_time_user;
    

    ivector v;
    v = icreavector(TAMVECTOR);
    cargarVectorValAleatorios(v, TAMVECTOR);
/*
 * 
    //EJEMPLO DE CLASE poner TAMVECTOR 11 Y USAR FUNCION quicksortSinUmbral Y DEFINIR PRINTF
    v[0] = 32;
    v[1] = 15;
    v[2] = 26;
    v[3] = 44;
    v[4] = 42;
    v[5] = 24;
    v[6] = 35;
    v[7] = 21;
    v[8] = 18;
    v[9] = 30;
    v[10] = 11;
*/
    
    //mostrarVector(v, TAMVECTOR);

    
    start = clock();
    
#ifdef ORDENACIONINSERCION
    ordenacionInsercion(v,0,TAMVECTOR-1);
#endif
    
#ifdef QUICKSORTUMBRAL
    quicksort(v, 0, TAMVECTOR -1); //Tiempo: 0.0000182  SIN UMBRAL
#endif
    
#ifdef QUICKSORTSINUMBRAL
    quicksortSinUmbral(v, 0, TAMVECTOR -1); //Tiempo: 0.0000182  SIN UMBRAL
#endif    
    
    end = clock();
    
    cpu_time_user = ((double) end - start) / CLOCKS_PER_SEC;
    
    printf("Tiempo: %f", cpu_time_user);

    //mostrarVector(v, TAMVECTOR);

    ifreevector(&v);
    
    return (EXIT_SUCCESS);
}


/**
 * @brief Ordenacion quicksort
 * @param v vector de enteros
 * @param i posicion de comienzo del vector
 * @param j posicion final de vector
 *        la i y la j, es para conocer la particion de cada una de las partes
 */
void quicksort (ivector v, int i, int j){
    int k, pivote;
    
//No es necesario esto del umbral, porque lo que hace es ordenarlo
    //Se podria tomar el tiempo para ver cuanto tarda

    //A partir de un umbral, a esa particion del vector lo ordenamos por el al
    //algoritmo de ordension por insercion
    if (j-i <= UMBRAL)
        ordenacionInsercion(v,i,j);//qsort (v+i,j-i,sizeof(int), compare);
    else{
        pivote = encuentraPivote(v, i, j);
        if (pivote != -1){
            k = reordena(v, i,j,v[pivote]);
            quicksort(v, i, k-1);
            quicksort(v, k, j);
        }
    }
}


/**
 * @brief Ordenacion quicksort sin umbral, si esta ordenado el encuntraPivote 
 *        devuelve -1 y termina 
 * @param v vector de enteros
 * @param i posicion de comienzo del vector
 * @param j posicion final de vector
 *        la i y la j, es para conocer la particion de cada una de las partes
 */
void quicksortSinUmbral (ivector v, int i, int j){
    int k, pivote;
    
//No es necesario esto del umbral, porque lo que hace es ordenarlo
    //Se podria tomar el tiempo para ver cuanto tarda

    
        pivote = encuentraPivote(v, i, j);
        if (pivote != -1){
            k = reordena(v, i,j,v[pivote]);
            quicksort(v, i, k-1);
            quicksort(v, k, j);
        } 
}



/**
 * @brief Empezando por la izquierda, devuelve la posicion 
 *        del mayor de los dos primeros elementos que sean distintos
 * @param v Vector de enteros
 * @param i 
 * @param j
 * @return Devuelve la poscion del pivote
 */
int encuentraPivote (ivector v, int i, int j){
    int k;
    
    for (k=i+1; k<=j;k++)
        if (v[k]>v[i])return k;
        else if (v[k]<v[i]) return i;
    
    return -1;
        
}

/**
 * @brief Deja los elementos menores que el pivote a la izquierda
 *        y los mayores o iguales a la derecha. Devuelve la posicion
 *        de corte
 * @param v 
 * @param i
 * @param d
 * @param pivote Valor del pivote
 * @return Devuelve la posicion de corte
 */
int reordena (ivector v, int i, int d, int pivote){
    
    do{
        int temp;
        temp = v[i];
        v[i] = v[d];
        v[d] = temp;

/*
#ifdef PRINTF 
    mostrarVector(v,TAMVECTOR);
#endif
*/
        while (v[i] < pivote) i++;
        while (v[d] >= pivote) d--;
    }while (i <= d);

#ifdef PRINTF 
    mostrarVector(v,TAMVECTOR);
#endif   


    return i;
}

/**
 * @brief Funcion para mostrar el vector
 * @param v vector de enteros
 * @param tam tamaño del vector
 */
void mostrarVector(ivector v, int tam){
    
    for (int i = 0; i< tam; i++)
        printf ("%d\t", v[i]);
    printf ("\n");
}


/**
 * @brief Metodo de comparacion para la funcion qsort de la libreria
 *        
 * @param _a
 * @param _b
 * @return 
 */
int compare(const void *_a, const void *_b) {
 
        int *a, *b;
        
        a = (int *) _a;
        b = (int *) _b;
        
        return (*a - *b);
}

/**
 * @brief Funcion para cargar un vector de entero de cualquier tamaño la semilla
 *        es 23 
 * @param v
 * @param tam
 */
void cargarVectorValAleatorios(ivector v, int tam){
    srand(23);//srand(time(NULL));
    for (int i= 0; i< tam; i++)
        v[i] = rand()%1000;
}


/**
 * @brief Algoritmo clasico de ordenacion por insercion
 *        Utilizado para ordenadar dado un umbral
 * @param v
 * @param i
 * @param j
 */
void ordenacionInsercion (ivector v, int i, int j){
    
    int p, k;
    int tmp;
    
    for (p = i; p <= j; p++){
        tmp = v[p];
        k = p-1; 
        while ((k>=0) && (tmp < v[k])){
            v[k+1] = v[k];
            k--;
        }
        v[k+1] = tmp;
    }
    
    
    
}